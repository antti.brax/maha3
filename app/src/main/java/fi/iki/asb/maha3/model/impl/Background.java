package fi.iki.asb.maha3.model.impl;

import android.graphics.Paint;

import fi.iki.asb.maha3.view.MyDrawable;
import fi.iki.asb.maha3.model.MyObject;
import fi.iki.asb.maha3.view.impl.Square;

public class Background implements MyObject {

    private final Square square = new Square();

    public Background() {
        square.getPaint().setARGB(255, 0, 0, 0);
        square.getPaint().setStyle(Paint.Style.FILL);
        square.getRect().set(-100.0f, -100.0f, 100.0f, 100.0f);
        square.setZ(-10000.0f);
    }

    @Override
    public MyDrawable getDrawable() {
        return square;
    }

}
