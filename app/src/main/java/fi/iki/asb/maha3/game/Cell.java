package fi.iki.asb.maha3.game;

public class Cell {

    private final int color;

    private boolean selected = false;

    public Cell(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
