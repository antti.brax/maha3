package fi.iki.asb.maha3.model;

public interface MyModelCallback {

    void addObject(MyObject ... object);

    void clearObjects();

    void removeObject(MyObject object);

}
