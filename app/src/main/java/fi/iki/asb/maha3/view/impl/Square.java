package fi.iki.asb.maha3.view.impl;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import fi.iki.asb.maha3.view.MyDrawable;

public class Square implements MyDrawable {

    private final Paint paint = new Paint();

    private final RectF rect = new RectF();

    private float z = 0.0f;

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRect(rect, paint);
    }

    public void setZ(float z) {
        this.z = z;
    }

    @Override
    public float getZ() {
        return z;
    }

    public Paint getPaint() {
        return paint;
    }

    public RectF getRect() {
        return rect;
    }
}
