package fi.iki.asb.maha3.game;

import java.util.function.Consumer;

public class Board {

    private final int width;
    private final int height;
    private final Cell[] grid;
    private Consumer<BoardEvent> boardEventListener;

    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        this.grid = new Cell[width * height];
        setBoardEventListener(null);
    }

    public void setBoardEventListener(Consumer<BoardEvent> boardEventListener) {
        if (boardEventListener != null) {
            this.boardEventListener = boardEventListener;
        } else {
            this.boardEventListener = e -> {};
        }
    }

    public void move(int fromX, int fromY, int toX, int toY) {
        if (! isEmpty(toX, toY)) {
            throw new IllegalArgumentException("[" + toX + "," + toY + "] is not empty");
        }

        Cell value = getCellAt(fromX, fromY);
        grid[i(fromX, fromY)] = null;
        grid[i(toX, toY)] = value;

        boardEventListener.accept(BoardEvent.moveCell(fromX, fromY, toX, toY));
    }

    public boolean isEmpty(int x, int y) {
        return getCellAt(x, y) == null;
    }

    public void setCellAt(int x, int y, Cell value) {
        grid[i(x, y)] = value;

        boardEventListener.accept(BoardEvent.placeCell(x, y));
    }

    public Cell getCellAt(int x, int y) {
        return grid[i(x, y)];
    }

    public boolean contains(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public void forEachCell(Consumer<Cell> consumer) {
        for (Cell c: grid) {
            if (c != null) {
                consumer.accept(c);
            }
        }
    }

    public void removeCellAt(int x, int y) {
        if (! isEmpty(x, y)) {
            grid[i(x, y)] = null;
            boardEventListener.accept(BoardEvent.removeCell(x, y));
        } else {
            throw new IllegalStateException();
        }
    }

    private int i(int x, int y) {
        return x + y * width;
    }
}
