package fi.iki.asb.maha3.model.impl;

import fi.iki.asb.maha3.game.BoardController;
import fi.iki.asb.maha3.model.MyObject;

public class BoardTouchListener implements MyObject {

    private final BoardController boardController;

    public BoardTouchListener(BoardController boardController) {
        this.boardController = boardController;
    }

    @Override
    public boolean contains(float x, float y) {
        return true;
    }

    @Override
    public boolean isTouchable() {
        return true;
    }

    @Override
    public void touch(float x, float y) {
        final int xx = (int) (x * 10f);
        final int yy = (int) (y * 10f);
        boardController.touch(xx, yy);
    }
}
