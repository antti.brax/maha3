package fi.iki.asb.maha3.model;

/**
 * Location interpolated between two temporal locations.
 */
public class InterpolatedTemporalLocation implements Location {

    private final TemporalLocation from;

    private final TemporalLocation to;

    public InterpolatedTemporalLocation(TemporalLocation from, TemporalLocation to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public float x() {
        final long now = System.currentTimeMillis();
        final long fromTime = from.getTime();
        final float fromX = from.x();
        if (now <= fromTime) {
            return fromX;
        }

        final long toTime = to.getTime();
        final float toX = to.x();
        if (now >= toTime) {
            return toX;
        }

        final float timeDelta = ( toTime - now ) / (float) ( toTime - fromTime );
        return toX - ( toX - fromX ) * timeDelta;
    }

    @Override
    public float y() {
        final long now = System.currentTimeMillis();
        final long fromTime = from.getTime();
        final float fromY = from.y();
        if (now <= fromTime) {
            return fromY;
        }

        final long toTime = to.getTime();
        final float toY = to.y();
        if (now >= toTime) {
            return toY;
        }

        final float timeDelta = ( toTime - now ) / (float) ( toTime - fromTime );
        return toY - ( toY - fromY ) * timeDelta;
    }
}
