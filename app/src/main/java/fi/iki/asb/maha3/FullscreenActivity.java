package fi.iki.asb.maha3;

import androidx.appcompat.app.AppCompatActivity;
import fi.iki.asb.maha3.game.Board;
import fi.iki.asb.maha3.game.BoardController;
import fi.iki.asb.maha3.model.MyModel;
import fi.iki.asb.maha3.model.impl.Background;
import fi.iki.asb.maha3.model.impl.GameInitializer;
import fi.iki.asb.maha3.model.impl.StartGameObject;
import fi.iki.asb.maha3.view.MyView;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_fullscreen);

        final MyModel model = new MyModel();
        model.addObject(new Background());
        model.addObject(new StartGameObject());

        final MyView view = (MyView) findViewById(R.id.fullscreen_content);
        view.setOnTouchListener(new MyTouchTranslator(model));

        final MyUpdateTimer timer = new MyUpdateTimer(view, model);
    }
}
