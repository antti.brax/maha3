package fi.iki.asb.maha3.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.SurfaceView;

import fi.iki.asb.maha3.R;

public class MyView extends SurfaceView {

    private final float aspectRatio;

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.MyView,
                0,
                0);

        try {
            aspectRatio = a.getFloat(R.styleable.MyView_aspectRatio, 1.0f);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int desiredWidth = MeasureSpec.getSize(widthMeasureSpec);
        final int desiredHeight = MeasureSpec.getSize(heightMeasureSpec);

        final int calculatedWidth = (int) (desiredHeight / aspectRatio);
        final int calculatedHeight = (int) (desiredWidth * aspectRatio);

        int width;
        int height;

        if (calculatedHeight > desiredHeight) {
            width = calculatedWidth;
            height = desiredHeight;
        } else if (calculatedWidth > desiredWidth) {
            width = desiredWidth;
            height = calculatedHeight;
        } else {
            width = calculatedWidth;
            height = calculatedHeight;
        }

        setMeasuredDimension(width, height);
    }
}
