package fi.iki.asb.maha3.view;

import android.graphics.Canvas;

public interface MyDrawable {

    default float getZ() {
        return 0.0f;
    }

    void draw(Canvas canvas);

}
