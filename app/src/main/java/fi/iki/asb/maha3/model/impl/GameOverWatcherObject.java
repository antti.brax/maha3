package fi.iki.asb.maha3.model.impl;

import fi.iki.asb.maha3.game.BoardController;
import fi.iki.asb.maha3.model.MyModelCallback;
import fi.iki.asb.maha3.model.MyObject;

/**
 * Object that checks if game has ended. When the game ends, this object
 * removes the object that listens to in game touches and adds an object
 * that starts a new game when touched (StartGameObject).
 */
public class GameOverWatcherObject implements MyObject {

    private final BoardController controller;

    private final MyObject remove;

    GameOverWatcherObject(BoardController controller, MyObject remove) {
        this.controller = controller;
        this.remove = remove;
    }

    @Override
    public void update(MyModelCallback callback) {
        if (controller.isGameOver()) {
            callback.removeObject(this);
            callback.removeObject(remove);
            callback.addObject(new StartGameObject());
        }
    }
}
