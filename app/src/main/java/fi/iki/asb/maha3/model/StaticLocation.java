package fi.iki.asb.maha3.model;

public class StaticLocation implements Location {

    private final float x;
    private final float y;

    public StaticLocation(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public float x() {
        return x;
    }

    @Override
    public float y() {
        return y;
    }
}
