package fi.iki.asb.maha3.model.impl;

import android.util.Log;

import fi.iki.asb.maha3.model.MyModelCallback;
import fi.iki.asb.maha3.model.MyObject;
import fi.iki.asb.maha3.view.MyDrawable;
import fi.iki.asb.maha3.view.impl.Square;

/**
 * Object that starts a new game when touched.
 */
public class StartGameObject implements MyObject {

    private final Square square = new Square();

    private boolean dead = false;

    public StartGameObject() {
        Log.d("XXX", "StartGameObject");

        square.getRect().set(0.1f, 0.1f, 0.9f, 1.3f);
        square.getPaint().setARGB(64, 255, 255, 255);
        square.getPaint().setAntiAlias(true);
        square.setZ(2.0f);
    }

    @Override
    public MyDrawable getDrawable() {
        return square;
    }

    @Override
    public boolean isTouchable() {
        return true;
    }

    @Override
    public boolean contains(float x, float y) {
        Log.d("XXX", "X:" + x + ", Y:" + y);
        return square.getRect().contains(x, y);
    }

    @Override
    public void update(MyModelCallback callback) {
        if (dead) {
            callback.addObject(new GameInitializer());
            callback.removeObject(this);
        }
    }

    @Override
    public void touch(float x, float y) {
        dead = true;
    }
}
