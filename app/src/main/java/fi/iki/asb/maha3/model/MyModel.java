package fi.iki.asb.maha3.model;

import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MyModel implements MyModelCallback {

    private List<MyObject> objects = new LinkedList<>();

    private ArrayList<MyObject> temp = new ArrayList<>(128);

    public MyModel() {
    }

    public void update() {
        temp.clear();
        temp.addAll(objects);
        temp.forEach(o -> o.update(this));
    }

    public MyObject getAt(float x, float y) {
        return objects.stream()
                .filter(MyObject::isTouchable)
                .filter(o -> o.contains(x, y))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void addObject(MyObject ... object) {
        for (MyObject o: object) {
            objects.add(o);
        }
    }

    @Override
    public void clearObjects() {
        objects.clear();
    }

    @Override
    public void removeObject(MyObject object) {
        objects.remove(object);
    }

    public void draw(Canvas canvas) {
        canvas.save();
        try {
            canvas.scale(canvas.getWidth(), canvas.getWidth());

            objects.stream()
                    .map(MyObject::getDrawable)
                    .sorted((o1, o2) -> Float.compare(o1.getZ(), o2.getZ()))
                    .forEach(o -> o.draw(canvas));
        } finally {
            canvas.restore();
        }
    }

}
