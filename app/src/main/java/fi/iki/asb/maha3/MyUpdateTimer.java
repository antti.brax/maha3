package fi.iki.asb.maha3;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import fi.iki.asb.maha3.model.MyModel;
import fi.iki.asb.maha3.view.MyView;

/**
 * This needs refactoring. Produce events. Have event listeners update model and draw view.
 */
public class MyUpdateTimer {

    private final MyView view;

    private final MyModel model;

    private MyRunnable runnable = null;

    public MyUpdateTimer(MyView view, MyModel model) {
        this.view = view;
        this.model = model;

        this.view.getHolder().addCallback(new MyCallback());
    }

    private synchronized void start() {
        if (runnable != null) {
            stop();
        }

        runnable = new MyRunnable();
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.start();
    }

    private synchronized void stop() {
        if (runnable != null) {
            runnable.stop = true;
            runnable = null;
        }
    }

    // =================================================================== //

    private class MyRunnable implements Runnable {

        private volatile boolean stop = false;

        @Override
        public void run() {
            long loopStartTime = System.currentTimeMillis();

            while (! stop) {
                repaint();
                model.update();

                try {
                    long endSleep = loopStartTime + 40;
                    long sleep = endSleep - System.currentTimeMillis();

                    if (sleep < 0) {
                        loopStartTime = endSleep - sleep;
                    } else {
                        loopStartTime = endSleep;
                        Thread.sleep(sleep);
                    }
                } catch (InterruptedException ex) {
                    return;
                }
            }
        }
    }

    private synchronized void repaint() {
        final Canvas canvas = view.getHolder().lockCanvas();
        try {
            model.draw(canvas);
        } finally {
            try {
                view.getHolder().unlockCanvasAndPost(canvas);
            } catch (IllegalStateException | IllegalArgumentException ex) {
                // We have been stopped. Ignore.
            }
        }
    }

    /**
     * Update thread is controlled by the surface.
     */
    private class MyCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            start();
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
            // NOP
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            stop();
        }
    }
}
