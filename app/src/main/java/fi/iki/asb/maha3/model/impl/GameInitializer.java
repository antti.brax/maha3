package fi.iki.asb.maha3.model.impl;

import fi.iki.asb.maha3.game.Board;
import fi.iki.asb.maha3.game.BoardController;
import fi.iki.asb.maha3.model.MyModelCallback;
import fi.iki.asb.maha3.model.MyObject;

/**
 * Object that initializes the game when it is updated
 * by the model and then immediately removes itself.
 */
public class GameInitializer implements MyObject {

    private final Board board;

    public GameInitializer() {
        this.board = new Board(10, 14);
    }

    @Override
    public void update(MyModelCallback callback) {
        final BoardController controller = new BoardController(board);
        final BoardTouchListener touchListener = new BoardTouchListener(controller);

        callback.clearObjects();
        callback.addObject(new Background());
        callback.addObject(new BoardEventListener(board));
        callback.addObject(touchListener);
        callback.addObject(new GameOverWatcherObject(controller, touchListener));
        controller.fill();
    }
}
