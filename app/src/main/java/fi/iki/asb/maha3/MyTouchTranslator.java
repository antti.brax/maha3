package fi.iki.asb.maha3;

import android.graphics.Matrix;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import fi.iki.asb.maha3.model.MyModel;
import fi.iki.asb.maha3.model.MyObject;

/**
 * Translate a touch event received by the component to
 * a method call to touch(float x, float y) on the object
 * under the event position.
 */
public class MyTouchTranslator implements View.OnTouchListener {

    private final MyModel model;

    public MyTouchTranslator(MyModel model) {
        this.model = model;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final float scale = 1.0f / view.getWidth();
        final Matrix m = new Matrix();
        m.preScale(scale, scale);
        motionEvent.transform(m);

        final MyObject object = model.getAt(motionEvent.getX(), motionEvent.getY());

        Log.d("XXX", "Touching " + object);

        if (object != null) {
            object.touch(motionEvent.getX(), motionEvent.getY());
        }

        return false;
    }
}
