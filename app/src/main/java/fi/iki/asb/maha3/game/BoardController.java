package fi.iki.asb.maha3.game;

import java.util.Random;

/**
 * Implement game logic.
 */
public class BoardController {

    private final Random random = new Random();

    private final Board board;

    private boolean gameOver = false;

    public BoardController(Board board) {
        this.board = board;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void touch(int x, int y) {
        Cell cell = board.getCellAt(x, y);
        if (cell == null) {
            return;
        }

        if (cell.isSelected()) {
            removeSelected();
            collapse();
            compact();
            checkRefill();
            checkGameOver();
        } else {
            selectAt(x, y);
        }
    }

    private void removeSelected() {
        for (int y = 0; y < 14; y++) {
            for (int x = 0; x < 10; x++) {
                final Cell cell = board.getCellAt(x, y);
                if (cell == null || !cell.isSelected()) {
                    continue;
                }

                board.removeCellAt(x, y);
            }
        }
    }

    public void fill() {
        for (int y = 0; y < 14; y++) {
            for (int x = 0; x < 10; x++) {
                final Cell cell = board.getCellAt(x, y);
                if (cell != null) {
                    continue;
                }

                board.setCellAt(x, y, new Cell(random.nextInt(5)));
            }
        }

        gameOver = false;
    }

    /**
     * Collapse blocks down.
     */
    private void collapse() {
        for (int x = 0; x < 10; x++) {
            for (int y2 = 13; y2 >= 1; y2--) {
                if (!board.isEmpty(x, y2)) {
                    continue;
                }

                for (int y1 = y2 - 1; y1 >= 0; y1--) {
                    if (board.isEmpty(x, y1)) {
                        continue;
                    }

                    board.move(x, y1, x, y2);
                    break;
                }
            }
        }
    }

    /**
     * Compact blocks left.
     */
    private void compact() {
        for (int x2 = 0; x2 < 9; x2++) {
            if (!board.isEmpty(x2, 13)) {
                continue;
            }

            for (int x1 = x2 + 1; x1 < 10; x1++) {
                if (board.isEmpty(x1, 13)) {
                    continue;
                }

                for (int y = 0; y < 14; y++) {
                    if (board.isEmpty(x1, y)) {
                        continue;
                    }

                    board.move(x1, y, x2, y);
                }

                break;
            }
        }
    }

    private void checkRefill() {
        if (board.isEmpty(0, 13)) {
            fill();
        }
    }

    private void checkGameOver() {
        for (int y = 0; y < 14; y++) {
            for (int x = 0; x < 9; x++) {
                Cell c1 = board.getCellAt(x, y);
                Cell c2 = board.getCellAt(x + 1, y);

                if (c1 != null && c2 != null && c1.getColor() == c2.getColor()) {
                    return;
                }
            }
        }

        for (int y = 0; y < 13; y++) {
            for (int x = 0; x < 10; x++) {
                Cell c1 = board.getCellAt(x, y);
                Cell c2 = board.getCellAt(x, y + 1);

                if (c1 != null && c2 != null && c1.getColor() == c2.getColor()) {
                    return;
                }
            }
        }

        gameOver = true;
    }

    private void selectAt(int x, int y) {
        board.forEachCell(c -> c.setSelected(false));

        final Cell cell = board.getCellAt(x, y);
        if (selectColorAt(x, y, cell.getColor()) == 1) {
            cell.setSelected(false);
        }
    }

    private int selectColorAt(int x, int y, int color) {
        if (! board.contains(x, y)) {
            return 0;
        }

        final Cell cell = board.getCellAt(x, y);
        if (cell == null || cell.isSelected() || cell.getColor() != color) {
            return 0;
        }

        cell.setSelected(true);
        int selected = 1;
        selected += selectColorAt(x - 1, y, color);
        selected += selectColorAt(x + 1, y, color);
        selected += selectColorAt(x, y - 1, color);
        selected += selectColorAt(x, y + 1, color);
        return selected;
    }

}
