package fi.iki.asb.maha3.model;

import fi.iki.asb.maha3.view.MyDrawable;

public interface MyObject {

    default void update(MyModelCallback callback) {
        // Do nothing.
    }

    /**
     * Called when this object is touched. Will never be called if isTouchable
     * returns false.
     */
    default void touch(float x, float y) {
        // Do nothing.
    }

    /**
     * Can this object be touched?
     */
    default boolean isTouchable() {
        return false;
    }

    default boolean contains(float x, float y) {
        return false;
    }

    default MyDrawable getDrawable() {
        return (c) -> {
            // Do nothing.
        };
    }
}
