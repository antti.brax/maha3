package fi.iki.asb.maha3.game;

public class BoardEvent {

    public enum EventType {
        CELL_PLACED,
        CELL_REMOVED,
        CELL_MOVED
    };

    private final EventType type;

    private final int[] params;

    private BoardEvent(EventType type, int ... params) {
        this.type = type;
        this.params = params;
    }

    public EventType getType() {
        return type;
    }

    public int get(int i) {
        return params[i];
    }

    public static BoardEvent placeCell(int x, int y) {
        return new BoardEvent(EventType.CELL_PLACED, x, y);
    }

    public static BoardEvent removeCell(int x, int y) {
        return new BoardEvent(EventType.CELL_REMOVED, x, y);
    }

    public static BoardEvent moveCell(int fromX, int fromY, int toX, int toY) {
        return new BoardEvent(EventType.CELL_MOVED, fromX, fromY, toX, toY);
    }
}
