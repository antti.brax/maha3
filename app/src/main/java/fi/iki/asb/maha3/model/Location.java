package fi.iki.asb.maha3.model;

public interface Location {

    float x();

    float y();

}
