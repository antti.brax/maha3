package fi.iki.asb.maha3.model;

public class TemporalLocation implements Location {

    private final long time;
    private final float x;
    private final float y;

    public TemporalLocation(long time, float x, float y) {
        this.time = time;
        this.x = x;
        this.y = y;
    }

    public long getTime() {
        return time;
    }

    @Override
    public float x() {
        return x;
    }

    @Override
    public float y() {
        return y;
    }
}
