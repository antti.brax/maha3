package fi.iki.asb.maha3.model.impl;

import android.graphics.Paint;

import fi.iki.asb.maha3.game.Cell;
import fi.iki.asb.maha3.model.Location;
import fi.iki.asb.maha3.model.MyModelCallback;
import fi.iki.asb.maha3.model.MyObject;
import fi.iki.asb.maha3.model.StaticLocation;
import fi.iki.asb.maha3.view.MyDrawable;
import fi.iki.asb.maha3.view.impl.Square;

public class CellObject implements MyObject {

    private final Square square = new Square();

    private final Cell cell;

    private Location location = new StaticLocation(0f, 0f);

    private boolean dead = false;

    public CellObject(Cell cell) {
        this.cell = cell;

        switch (cell.getColor()) {
            case 0:
                square.getPaint().setARGB(255, 255, 0, 0);
                break;
            case 1:
                square.getPaint().setARGB(255, 0, 255, 0);
                break;
            case 2:
                square.getPaint().setARGB(255, 0, 0, 255);
                break;
            case 3:
                square.getPaint().setARGB(255, 255, 0, 255);
                break;
            case 4:
                square.getPaint().setARGB(255, 255, 255, 0);
                break;
            default:
                throw new IllegalArgumentException("" + cell.getColor());
        }

        square.getPaint().setStyle(Paint.Style.FILL);
        square.setZ(1.0f);
    }

    public void set(Location location) {
        if (location == null) {
            throw new NullPointerException();
        }
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    @Override
    public void update(MyModelCallback callback) {
        if (dead) {
            callback.removeObject(this);
        } else {
            square.getRect().set(
                    location.x() + 0.001f, location.y() + 0.001f,
                    location.x() + 0.099f, location.y() + 0.099f);
        }
    }

    @Override
    public MyDrawable getDrawable() {

        if (cell.isSelected()) {
            square.getPaint().setAlpha(127);
        } else {
            square.getPaint().setAlpha(255);
        }

        return square;
    }
}
