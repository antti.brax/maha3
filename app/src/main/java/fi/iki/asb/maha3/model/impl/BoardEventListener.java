package fi.iki.asb.maha3.model.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import fi.iki.asb.maha3.game.Board;
import fi.iki.asb.maha3.game.BoardEvent;
import fi.iki.asb.maha3.model.InterpolatedTemporalLocation;
import fi.iki.asb.maha3.model.Location;
import fi.iki.asb.maha3.model.MyModelCallback;
import fi.iki.asb.maha3.model.MyObject;
import fi.iki.asb.maha3.model.StaticLocation;
import fi.iki.asb.maha3.model.TemporalLocation;

/**
 * Listen to board events and update the model accordingly.
 */
public class BoardEventListener implements MyObject, Consumer<BoardEvent> {

    private final Board board;

    /**
     * Keys are x,y locations on board. Values are objects in model.
     */
    private final Map<String, CellObject> objects = new HashMap<>();

    private final List<MyObject> pending = new ArrayList<>();

    public BoardEventListener(Board board) {
        this.board = board;
        board.setBoardEventListener(this);
    }

    @Override
    public void accept(BoardEvent boardEvent) {
        switch (boardEvent.getType()) {
            case CELL_MOVED:
                move(boardEvent.get(0), boardEvent.get(1),
                        boardEvent.get(2), boardEvent.get(3));
                break;
            case CELL_PLACED:
                place(boardEvent.get(0), boardEvent.get(1));
                break;
            case CELL_REMOVED:
                remove(boardEvent.get(0), boardEvent.get(1));
                break;
        }
    }

    @Override
    public void update(MyModelCallback callback) {
        pending.forEach(callback::addObject);
        pending.clear();
    }

    private void move(int fromX, int fromY, int toX, int toY) {
        final CellObject cell = objects.remove(key(fromX, fromY));
        if (cell == null) {
            throw new IllegalArgumentException("No cell at [" + fromX + "," + fromY + "]");
        }

        final Location loc = cell.getLocation();
        final long now = System.currentTimeMillis();
        TemporalLocation from = new TemporalLocation(now, loc.x(), loc.y());
        TemporalLocation to = new TemporalLocation(now + 150, toX / 10f, toY / 10f);

        cell.set(new InterpolatedTemporalLocation(from, to));

        objects.put(key(toX, toY), cell);
    }

    private void place(int x, int y) {
        final CellObject cell = new CellObject(board.getCellAt(x, y));
        cell.set(new StaticLocation(x / 10f, y / 10f));

        objects.put(key(x, y), cell);
        pending.add(cell);
    }

    private void remove(int x, int y) {
        final CellObject cell = objects.get(key(x, y));
        if (cell == null) {
            throw new IllegalArgumentException("No cell at [" + x + "," + y + "]");
        }

        cell.setDead(true);
    }

    private String key(int x, int y) {
        return x + "," + y;
    }

}
